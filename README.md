# bootstrap

`curl https://gitlab.com/dockercitalk/bootstrap/raw/master/bootstrap | sudo bash`

Then do:

`git clone https://gitlab.com/dockercitalk/packer.git`

`git clone https://gitlab.com/dockercitalk/terraform.git`

`git clone https://gitlab.com/dockercitalk/docker.git`

This script is designed to work with debian stretch.
